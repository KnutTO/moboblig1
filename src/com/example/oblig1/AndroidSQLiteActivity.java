package com.example.oblig1;

//import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
//import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class AndroidSQLiteActivity extends Activity{
	int amountUsers=0; //default is 0 users in database
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_database);
		
		DatabaseHandler db = new DatabaseHandler(this); //Opens database
		
		this.amountUsers=db.getUsersCount(); //counts users for the private variable
		updateCount(this.amountUsers); //prints amount of users bottom of screen
		
		
		/*List<User> users=db.getAllUsers(); //Writes all users to debug, currently the way to see all entries
		db.close();
		for (User u : users){
			String log = "Id: "+u.getID()+" ,Name: "+u.getName();
			Log.d("Name: ", log);
		}*/
	}
	
	public void addUser(View view){
		EditText tempUser= (EditText) findViewById(R.id.addUser); //takes in a username
		String user= tempUser.getText().toString();				//and makes it a string
		
		DatabaseHandler db = new DatabaseHandler(this); //opens database
		db.addUser(new User(user));						//Puts in new user
		db.close();
		
		this.amountUsers++;								//cheap way to update amount, instead of recounting
		updateCount(this.amountUsers);					//updates on screen
	}
	
	public void updateCount(int amount){
		String showUsers=""; 
		if (amount < 10){ //pads a 0 if less then 10 entries
			showUsers+="0";
		}
		showUsers+=String.valueOf(amount)+ " users"; //makes the text to show, i
		((TextView)findViewById(R.id.amount)).setText(showUsers); //puts on screen
	}
	
	public void findUser(View view){
		int tempID=-1; //makes invalid ID as default
		tempID=Integer.parseInt(((TextView)findViewById(R.id.giveID)).getText().toString()); //Gets string from textfield and converts to int 
		if (tempID <1 || tempID > this.amountUsers){		//If user out of bounds - assumes no deleted users
			((TextView)findViewById(R.id.userFound)).setText("INVALID USER!");		//Prints message that user does not exists.
		}
		else {
			DatabaseHandler db = new DatabaseHandler(this);
			User tempUser=db.getUser(tempID); //Gets user
			((TextView)findViewById(R.id.userFound)).setText(tempUser.getName()); //Prints on screen
		}
	}
	
	public void returnMain(View view){ //returns to main
		finish();
		Intent i=new Intent(AndroidSQLiteActivity.this, MainActivity.class);
		startActivity(i);
	}

}
