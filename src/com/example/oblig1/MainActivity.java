package com.example.oblig1;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    public void goTimer(View view) { //goes to timer
    	finish();
    	Intent i = new Intent(MainActivity.this, Timer.class);
    	startActivity(i);
    }
    
    public void downloadPicture(View view){ //Goes to picturedownload
    	finish();
    	Intent i = new Intent(MainActivity.this, Picture.class);
    	startActivity(i);
    }

    public void goDatabase(View view){ //Goes to database
    	finish();
    	Intent i = new Intent(MainActivity.this, AndroidSQLiteActivity.class);
    	startActivity(i);
    }
    
}
