package com.example.oblig1;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class Timer extends Activity {

	private long startTime;
	private long elapsedTime;
	private final int REFRESH_RATE=100;
	private String hours="00", minutes="00", seconds="00";
	private String countedTime;
	private long tempHours, tempMinutes, tempSeconds;
	private boolean stopped=false;
	
	private Handler mHandler = new Handler(); //To handle and schedule processes and the delay
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_timer);
	}
	
	
	@Override 
	public void onPause(){ //When paused, saves the counted time so far
		
		SharedPreferences state = getSharedPreferences("state", MODE_PRIVATE);
		SharedPreferences.Editor edit = state.edit();
		edit.putString("prev_time", countedTime); //Puts the string to variable
		edit.commit();
		super.onPause();
	}
	
	@Override
	public void onResume(){ //When resumed
		super.onResume();
		SharedPreferences state = getSharedPreferences("state", MODE_PRIVATE);
		countedTime = state.getString("prev_time", "00:00:00"); //Captures the previous time, if not found, puts 00:00:00 as default
		((TextView)findViewById(R.id.time)).setText(countedTime+" - previous time"); //prints the previous time with info
	}
	
	@Override
	public void onSaveInstanceState(Bundle savedInstanceState){
		mHandler.removeCallbacks(startTimer); //Stops timer when app loses focus due to timeout or pressing home-button
		stopped=true;						  //illogical to do so in a stopwatch, but more a case of showing possibility
	}
	
	public void returnMain(View view){ //Returns to main
		finish();
		Intent i=new Intent(Timer.this, MainActivity.class);
		startActivity(i);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.secondary, menu);
		return true;
	}
	
	private void updateTimer(float time){
		//Time is given in milliseconds to to convert to "readable" time:
		tempSeconds = (long) (time/1000)%60;	 //From milliseconds to seconds
		tempMinutes = (long)(time/60000)%60;	 //From milliseconds to minutes
		tempHours = (long)(time/3600000);	 //From milliseconds to hours
		
		//Converts to  string and adds padded 0's if needed:
		seconds=String.valueOf(tempSeconds);
		if (tempSeconds==0){
			seconds="00";
		}
		else if (tempSeconds < 10){
			seconds="0"+seconds;
		}
		
		minutes=String.valueOf(tempMinutes);
		if (tempMinutes==0){
			minutes="00";
		}
		else if (tempMinutes < 10){
			minutes="0"+minutes;
		}
		
		hours=String.valueOf(tempHours);
		if(tempHours==0){
			hours="00";
		}
		else if(tempHours<10){
			hours="0"+hours;
		}
		countedTime=hours+":"+minutes+":"+seconds;
		((TextView)findViewById(R.id.time)).setText(countedTime); //prints the time to the field
		
	}
	
	private Runnable startTimer=new Runnable() { //Starts the timer
		public void run() {
			elapsedTime=System.currentTimeMillis() - startTime; 
			updateTimer(elapsedTime);
			mHandler.postDelayed(this,REFRESH_RATE); //schedules next update 100ms
		}
	};
	
	public void startClick(View view){
		if(stopped){
			startTime=System.currentTimeMillis()-elapsedTime; 
		}
		else{
			startTime=System.currentTimeMillis();
		}
		mHandler.removeCallbacks(startTimer); //removes pending posts of startTimer
		mHandler.postDelayed(startTimer, 0); //makes the startTimer to be added right away, can be delayed if wanted
	}
	
	public void stopClick(View view){ //stops the timer
		mHandler.removeCallbacks(startTimer);
		stopped=true;
		((TextView)findViewById(R.id.time)).setText(hours+":"+minutes+":"+seconds);
	}
	
	public void resetClick(View view){ //resets timer
		stopped=false;
		((TextView)findViewById(R.id.time)).setText("00:00:00");
	}
}

