package com.example.oblig1;

//User class for the database with normal get's and set's
public class User{
	int id;
	String name;
	
	public User(){ //empty constructor
	}
	
	public User(int temp_id, String temp_name){
		this.id=temp_id;
		this.name=temp_name;
	}
	
	public User(String temp_name){
		this.name=temp_name;
	}
	
	public int getID(){
		return this.id;
	}
	
	public void setID(int temp_id){
		this.id=temp_id;
	}
	
	public String getName(){
		return this.name;
	}
	
	public void setName(String temp_name){
		this.name=temp_name;
	}
}
