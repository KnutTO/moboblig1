package com.example.oblig1;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


// A class to handle the communication with the database to add easy access from AndroidSQLiteActivity
// Or if others want to interact iwth the database.
public class DatabaseHandler extends SQLiteOpenHelper {

	
	private static final int DATABASE_VERSION=1;
	private static final String DATABASE_NAME="userManager";
	private static final String TABLE_USERS="users";
	
	private static final String KEY_ID="id";
	private static final String KEY_NAME="name";
	
	public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
	


	@Override
	public void onCreate(SQLiteDatabase db){
		String CREATE_USERS_TABLE = "CREATE TABLE " + TABLE_USERS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + KEY_NAME + " TEXT)";
		db.execSQL(CREATE_USERS_TABLE);

	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
	    db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
	 
	    // Create tables again
	    onCreate(db);
	}
	
	// Adding new contact
	public void addUser(User user) {
		SQLiteDatabase db=this.getWritableDatabase();
		
		ContentValues values= new ContentValues();
		values.put(KEY_NAME, user.getName()); //Users name
		
		//Insert row
		db.insert(TABLE_USERS, null, values);

	}
	 
	// Getting single contact
	public User getUser(int id) {
		SQLiteDatabase db = this.getReadableDatabase();
		
		Cursor cursor = db.query(TABLE_USERS, new String[] {KEY_ID, KEY_NAME}, KEY_ID + "=?",
				new String[] {String.valueOf(id)}, null, null, null, null);
		if (cursor!=null){
			cursor.moveToFirst();
		}
		User user = new User(Integer.parseInt(cursor.getString(0)),cursor.getString(1)); //makes user based on result
		
		return user;
	}
	 
	// Getting All Contacts
	public List<User> getAllUsers() {
		List<User> userList=new ArrayList<User>();
		
		String selectQuery = "SELECT * FROM " + TABLE_USERS;
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null); //requests all users
		
		if (cursor.moveToFirst()) {
			do { //goes through every row and makes a user to add ot list
				User user = new User();
				user.setID(Integer.parseInt(cursor.getString(0)));
				user.setName(cursor.getString(1));
				// Adding contact to list
				userList.add(user);
			} while (cursor.moveToNext());
		}
		
		return userList;
		
	}
	 
	// Getting contacts Count
	public int getUsersCount() {
		String countQuery= "SELECT * FROM " + TABLE_USERS;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery,  null);
		int amount=cursor.getCount();
		cursor.close();
		
		return amount;
		
	}
	// Updating single contact
	/* Not implemented functionality to update a user or delete a user.
	public int updateUser(User user) {
		return 0;
	}
	 
	// Deleting single contact
	public void deleteUser(User user) {
		
	}*/

	
}
